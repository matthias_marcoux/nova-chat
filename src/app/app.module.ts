import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './mainComponent/app.component';
import { Chat } from './chatComponent/chat.component';
import { Message } from './messagesComponent/message/message.component';
import { MessagesList } from './messagesComponent/messageList/messages.component';
import { FriendsList } from './friendsComponent/friendList/friends.component';
import { Friend } from './friendsComponent/friend/friend.component';
import { newMessage } from './messagesComponent/newMessage/newMessage.component';
import { FriendsDemandsList } from './friendsComponent/friendDemandList/friendsDemands.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ReadMessage } from './messagesComponent/readMessage/readMessage.component';
import { RecorderComponent } from './recorderComponent/recorder.component';
import { GameComponent } from './game/game.component';

@NgModule({
  declarations: [
    AppComponent,
    Chat,
    MessagesList,
    Message,
    FriendsList,
    Friend,
    newMessage,
    FriendsDemandsList,
    ReadMessage,
    RecorderComponent,
    GameComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [
    GameComponent
  ],
  bootstrap: [GameComponent],
  schemas: [
    NO_ERRORS_SCHEMA
  ]
})
export class AppModule { }
