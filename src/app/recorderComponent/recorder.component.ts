import { ChangeDetectorRef, Component } from '@angular/core';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';

@Component({
  selector: 'recorder-component',
  templateUrl: './recorder.component.html',
  styleUrls: ['./recorder.component.css']
})
export class RecorderComponent{
  constructor(private cdr:ChangeDetectorRef, private sanitizer:DomSanitizer){}

  videoChunks:Blob[] = [];
  mediaRecorder:MediaRecorder|null = null;
  videoSource:SafeUrl = "";
  disabled:boolean = false;
  href:SafeUrl = "";
  videoName:string = "";
  recording:boolean = false;
  controls:boolean = false;

  async startRecording() {
    this.videoChunks = [];
    this.disabled = true;

    try {
      const stream = await this.captureWebpage();
      this.mediaRecorder = new MediaRecorder(stream);

      this.mediaRecorder.ondataavailable = (event) => {
        if (event.data.size > 0) {
          this.videoChunks.push(event.data);
        }
      };

      this.mediaRecorder.onstop = () => {
        const superBuffer = new Blob(this.videoChunks, { type: 'video/mp4' });
        const url = URL.createObjectURL(superBuffer);
        this.videoSource = this.sanitizer.bypassSecurityTrustUrl(url);
        this.href = this.sanitizer.bypassSecurityTrustUrl(url);

        const date = new Date();
        this.videoName = `nova_raider_recording_${date.getFullYear()}-${date.getMonth()+1}-${date.getDate()}_${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}.mp4`;
        this.recording = false;
        this.controls = true;
        this.cdr.detectChanges();
      };

      this.mediaRecorder.start();
      this.recording = true;
      this.cdr.detectChanges();
    } catch (error) {
      console.error('Error starting recording:', error);
    }
  }

  captureWebpage() {
    const mediaOptions = { video: true, audio: true };
    return navigator.mediaDevices.getDisplayMedia(mediaOptions);
  }

  stopRecording(){
    this.mediaRecorder?.stop();
  }
}
