import { Component, Input } from "@angular/core";
import { ChatsService } from "src/app/services/chats/chats.service";

@Component({
  selector: 'friend',
  templateUrl: './friend.component.html',
  styleUrls: ['./friend.component.css']
})
export class Friend {
  constructor(private chatService:ChatsService){}

  @Input() name = '';
  @Input() status = false;

  addChat(){
    if(!this.chatService.chats.includes(this.name) && this.status){
      this.chatService.chats.push(this.name);
      this.chatService.openedTab = "chatIcon";
    }

    if(this.chatService.chats.includes(this.name) && this.status){
      this.chatService.openedTab = "chatIcon";
    }
  }
}
