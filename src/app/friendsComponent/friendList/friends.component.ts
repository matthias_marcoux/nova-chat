import { Component, Input } from "@angular/core";

@Component({
  selector: 'friends-component',
  templateUrl: './friends.component.html',
  styleUrls: ['./friends.component.css']
})
export class FriendsList {
  friendsList = [
    {
      name: 'Metale',
      status: true
    },
    {
      name: 'Number',
      status: false
    },
    {
      name: 'Sayo',
      status: false
    }
  ]
}
