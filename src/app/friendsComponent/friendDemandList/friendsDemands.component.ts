import { Component, Input } from "@angular/core";

@Component({
  selector: 'friends-demands-component',
  templateUrl: './friendsDemands.component.html',
  styleUrls: ['./friendsDemands.component.css']
})
export class FriendsDemandsList {
  friendsDemandsList = [
    {
      name: 'Metale',
      status: true
    },
    {
      name: 'Number',
      status: false
    },
    {
      name: 'Sayo',
      status: false
    }
  ]
}
