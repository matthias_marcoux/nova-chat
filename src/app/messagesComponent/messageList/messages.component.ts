import { Component, Input } from "@angular/core";
import { MessagesService } from "src/app/services/messages/messages.service";

@Component({
  selector: 'messages-component',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesList {
  constructor(private messagesService:MessagesService){}
  newMessage = false;
  readMessage = false;
  list = false;

  /* API récup tous les msg */
  allMsgsList = [
    {
      sender: 'Metale',
      receivers: [
        'Metale',
        'Starki',
        'Nexam'
      ],
      subject: 'Yo je suis en train de faire des tests',
      content : [],
      date: '02/10/2023',
      read: false
    },
    {
      sender: 'Number',
      receivers: [
        'Metale'
      ],
      subject: 'Alors ce chat ça avance ?',
      content : [
        {
          sender: 'Metale',
          content: 'Alors ce chat ça avance ?',
          date: '02/10/2023',
          hour: '10h12'
        },
        {
          sender: 'Number',
          content: 'Yes ça avance plutôt bien',
          date: '02/10/2023',
          hour: '10h15'
        }
      ],
      date: '01/10/2023',
      read: true
    },
    {
      sender: 'Sayo',
      receivers: [
        'Metale'
      ],
      subject: 'Pourquoi tu joue pas ?',
      content : [],
      date: '02/10/2023',
      read: true
    }
  ]

  sentMsgList = [
    {
      sender: 'Metale',
      receivers: [
        'Metale',
        'Starki',
        'Nexam'
      ],
      subject: 'Yo je suis en train de faire des tests',
      content : [],
      date: '02/10/2023',
      read: false
    },
  ]

  msgsList = this.allMsgsList;

  openMessage(i:number){
    this.list = false;
    this.newMessage = false;
    this.readMessage = true;

    this.messagesService.messageInfos = {
      subject: this.msgsList[i].subject,
      receivers: this.msgsList[i].receivers,
      content: this.msgsList[i].content
    }
  }

  openNewMessage(e:Event){
    this.list = false;
    this.readMessage = false;
    this.newMessage = true;

    this.activeItem(e);
  }

  openMessageList(e:Event){
    this.newMessage = false;
    this.readMessage = false;
    this.list = true;
    this.msgsList = this.allMsgsList;

    this.activeItem(e);
  }

  openSentMessageList(e:Event){
    this.newMessage = false;
    this.readMessage = false;
    this.list = true;
    this.msgsList = this.sentMsgList;

    this.activeItem(e);
  }

  activeItem(e:Event){
    document.querySelectorAll('.messagesOptions .active').forEach(e => e.classList.remove('active'));
    let element = e.target as Element;
    element.classList.add('active');
  }
}

