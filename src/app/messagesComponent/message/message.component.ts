import { Component, Input } from "@angular/core";

@Component({
  selector: 'message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class Message {
  @Input() sender:string = '';
  @Input() content:{}[] = [];
  @Input() subject:string = '';
  @Input() date:string = '';
  @Input() read:boolean = false;
}
