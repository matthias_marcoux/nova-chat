import { Component } from "@angular/core";
import { MessagesService } from "src/app/services/messages/messages.service";

@Component({
  selector: 'read-message',
  templateUrl: './readMessage.component.html',
  styleUrls: ['./readMessage.component.css']
})
export class ReadMessage {
  constructor(private messagesService:MessagesService){}

  subject = this.messagesService.messageInfos.subject;
  receivers = this.messagesService.messageInfos.receivers;
  content = this.messagesService.messageInfos.content;
}
