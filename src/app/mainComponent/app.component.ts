import { Component, DoCheck, HostListener } from '@angular/core';
import { ChatsService } from '../services/chats/chats.service';

@Component({
  selector: 'angular-nova-chat',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements DoCheck{
  constructor(private chatsService:ChatsService){}
  chatList: string[] = this.chatsService.chats;
  openedTab = this.chatsService.openedTab;
  nbChatNotif:number = this.chatsService.nbChatNotif;
  nbFriendsNotif:number = this.chatsService.nbFriendsNotif;
  nbMessagesNotif:number = this.chatsService.nbMessagesNotif;
  discord:boolean = false;
  isResizing:boolean = false;
  transition:{} = {
    transition: 'all 1s ease'
  }
  gameSize:string = '';

  ngDoCheck(): void {
    let tab = this.chatsService.openedTab;
    if(tab !== this.openedTab){
      let icon = document.getElementById(tab) as Element;
      this.onTabChange(icon);
      this.openedTab = tab;
    }
  }

  retract(){
    let chat = document.getElementById('chatContainer');
    let game = document.getElementById('game');
    if(chat?.classList.contains('retracted')){
      chat?.classList.remove('retracted');
      if(game) {
        game.classList.remove('retracted');
        game.style.width = this.gameSize;
        game.classList.add('transition');
        setTimeout(() => {
          game?.classList.remove('transition');
        }, 1000)
      }
    }
    else{
      chat?.classList.add('retracted');
      if(game){
        this.gameSize = game.style.width;
        game.classList.add('retracted');
        game.style.width = "100%";
      }
    }

  }

  active(event:Event){
    let icon = event.target as Element;
    this.chatsService.updateTab(icon.id);
    this.discord = false;
    let chat = document.getElementById('chatContainer') as Element

    if(icon.id == 'discordIcon'){
      this.discord = true;
      chat.classList.add('discordOpen');
    }
    else{
      if(chat.classList.contains('discordOpen')){
        chat.classList.add('nodelay');
        chat.classList.remove('discordOpen');
        setTimeout(() => {
          chat.classList.remove('nodelay');
        }, 1000)
      }
    }
  }

  onTabChange(icon:Element){
    let icons = document.querySelectorAll('.icon');
    icons.forEach(e => e.classList.remove('active'));
    icon.classList.add('active');
    this.updateNotifs();
  }

  updateNotifs(){
    this.nbChatNotif = this.chatsService.nbChatNotif;
    this.nbFriendsNotif = this.chatsService.nbFriendsNotif;
    this.nbMessagesNotif = this.chatsService.nbMessagesNotif;
  }

  @HostListener('document:mousemove', ['$event'])
  onMouseMove(event: MouseEvent) {
    if (this.isResizing) {
      requestAnimationFrame(() => {
        const gameElement = document.getElementById('game');
        const chatElement = document.getElementById('chatContainer');
        const wrapperElement = document.querySelector('.wrapper');

        this.transition = {
          transition: '0s'
        }

        if (gameElement && chatElement) {
          const newWidth = event.clientX;
          let chatWidth = `calc(${wrapperElement?.clientWidth}px - ${newWidth}px - 5px)`;
          chatElement.style.width = chatWidth;
          gameElement.style.width = `calc(100% - ${chatWidth} - 5px)`;
        }
      });
    }
  }

  @HostListener('document:mouseup', [])
  onMouseUp() {
    this.isResizing = false;
    this.transition = {
      transition: 'all 1s ease'
    }
  }

  startResize(event: MouseEvent) {
    this.isResizing = true;
    event.preventDefault();
  }
}
