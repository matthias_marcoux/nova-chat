import { Component, ChangeDetectorRef, Input, HostListener } from "@angular/core";

@Component({
  selector: 'chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class Chat {
  constructor(private cdr: ChangeDetectorRef){}

  @Input() name:string = '';
  @Input() index:number = 100;

  chatHeight:number = 0;
  mousePosition:number = 0;
  transition:{} = {
    transition: 'all 0.5s ease'
  }

  openClass = '';
  tmpSender:String = '';
  tmpCount:number = 0;
  down:Boolean = false;
  inited = false;

  addMsg(){
    let message = {
      hour: '7:30',
      sender: 'Metale',
      content: 'Yo ! Comment ça va ?',
      admin: false,
      mod: true
    }

    this.messages.push(message)
    this.tmpSender = message.sender;

    this.cdr.detectChanges();

    let chatContainer = document.querySelector('#chat-'+this.name+' .messages');
    chatContainer?.scrollTo(0, chatContainer.scrollHeight);
  }

  showHourAndSender(msg: Message, index: number){
    if (index === 0 || msg.sender !== this.messages[index - 1].sender) {
      return true;
    }
    return false;
  }

  /* mock data, recupère ton json/chat */
  messages:Message[] = [
    {
      hour: '7:30',
      sender: 'Metale',
      content: 'Yo ! Comment ça va ?',
      admin: false,
      mod: true
    },
    {
      hour: '7:30',
      sender: 'Sayo',
      content: 'Yo ! ça va bien et toi ?',
      admin: false,
      mod: false
    },
    {
      hour: '7:31',
      sender: 'Metale',
      content: 'Tranquille tranquille',
      admin: false,
      mod: true
    },
    {
      hour: '7:31',
      sender: 'Fladolf',
      content: 'Yo !',
      admin: false,
      mod: false
    },
    {
      hour: '7:32',
      sender: 'Metale',
      content: 'Salut Fladolf !',
      admin: false,
      mod: true
    },
    {
      hour: '7:33',
      sender: 'Sayo',
      content: 'Salut !',
      admin: false,
      mod: false
    },
    {
      hour: '7:34',
      sender: 'Sayo',
      content: 'Comment tu vas ?',
      admin: false,
      mod: false
    },
    {
      hour: '7:34',
      sender: 'Anon',
      content: 'Je suis un test de message un peu plus long pour voir le comportement',
      admin: false,
      mod: false
    },
    {
      hour: '7:35',
      sender: 'Fladolf',
      content: 'Niquel !',
      admin: false,
      mod: false
    }
  ]

  notifCount = this.messages.length;
  canClick:Boolean = true;

  changeSender(sender:String){
    if(!this.inited) this.tmpSender = sender;
  }

  ngAfterContentInit(){
    this.inited = true;
  }

  open(){
    if(this.canClick){
      this.openClass = this.openClass === 'open' ? '' : 'open';

      let chatBox = document.getElementById('chat-'+this.name) as Element;
      if(this.openClass == 'open'){
        chatBox.querySelectorAll('.msg').forEach(e => e.classList.remove('hidden'));
        this.chatHeight = 187;

        let messages = chatBox.querySelector('.messages');
        messages?.scrollTo(0, messages.scrollHeight);
      }
      else{
        this.chatHeight = 0;
        this.canClick = false;
        setTimeout(() => {
          chatBox.querySelectorAll('.msg').forEach(e => e.classList.add('hidden'));
          this.canClick = true;
        }, 500)
      }
    }
  }

  mouseDown(event:MouseEvent){
      this.down = true;
      this.mousePosition = event.clientY;

      this.transition = {
        transition: '0s'
      }

      let box = event.target as Element;
      box.classList.add('grow');
  }

  @HostListener('document:mouseup', ['$event'])
  mouseUp(event:MouseEvent){
    this.down = false;

    this.transition = {
      transition: 'all 0.5s ease'
    }

    let box = event.target as Element;
    box.classList.remove('grow');
  }

  @HostListener('document:mousemove', ['$event'])
  handleDocumentMouseMove(event: MouseEvent) {
    if(!this.down) return;

    const dy = this.mousePosition - event.y;
    this.mousePosition = event.y;
    this.chatHeight = this.chatHeight - dy;
  }
}

interface Message{
  hour:String;
  sender:String;
  content:String;
  admin:Boolean;
  mod:Boolean;
}
