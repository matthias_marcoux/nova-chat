import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MessagesService {
  messageInfos:MessageInfos = {
    subject: null,
    receivers: null,
    content : null,
  }
}

interface MessageInfos{
  subject:String|null;
  receivers:String[]|null;
  content:Message[]|null;
}

interface Message{
  sender:String;
  content:String,
  date:String;
  hour:String;
}
