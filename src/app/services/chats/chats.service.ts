import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class ChatsService {
  nbChatNotif:number = 1;
  nbFriendsNotif:number = 0;
  nbMessagesNotif:number = 12;

  public chats:string[] = [
    'English',
    'Français',
    'Deutsch',
    'Turkçe',
    'Italiano',
    'Espanol',
    'Polski'
  ];

  openedTab:string = "chatIcon";

  updateTab(newValue:string){
    this.openedTab = newValue;

    /*let value = newValue.split('Icon')[0];

    if(value === "chat") this.nbChatNotif = 0;
    if(value === "friends") this.nbFriendsNotif = 0;
    if(value === "messages") this.nbMessagesNotif = 0;*/
  }
}
